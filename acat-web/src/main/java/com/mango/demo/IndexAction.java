package com.mango.demo;

import com.mango.acat.core.annotation.ReqKey;
import com.mango.acat.core.base.BaseAction;
import com.mango.acat.core.dataset.DataSet;
import com.mango.acat.core.dataset.ParaSet;

@ReqKey("/")
public class IndexAction extends BaseAction{
	//跳转页面
	public DataSet index(ParaSet ps){
		return freemarker("/index.html");
	}
	//测试跳转freemarker
	public DataSet testFreemarker(ParaSet ps){
		this.setSessionAttr("root", ps.getStr("host"));
		return freemarker("/public/free.html");
	}
	
	//测试跳转freemarker2
	public DataSet test2(ParaSet ps){
		return freemarker("/public/free.html");
	}
	
	//下载文件
	public DataSet df(ParaSet ps){
		return file(ps.getStr("path"));
	}
}
