package com.mango.acat.core.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.stream.ChunkedWriteHandler;

public class AcatHttpServerInitializer extends ChannelInitializer<SocketChannel> {
	// private boolean isSSL = false;
	
	private SslContext sslCtx;
	
	public AcatHttpServerInitializer(SslContext sslCtx) {
		this.sslCtx = sslCtx;
		//this.maxSize = FrameConfig.getInt(FrameConfig.ConfigEnum.MAX_CONTENT_LENGTH);
	}

	@Override
	public void initChannel(SocketChannel ch) throws Exception {
		/*
		 * if (isSSL) { SSLEngine engine =
		 * SecureChatSslContextFactory.getServerContext().createSSLEngine();
		 * engine.setUseClientMode(false); pipeline.addLast("ssl", new
		 * SslHandler(engine)); }
		 */

		ch.pipeline().addLast(new HttpServerCodec());
		ch.pipeline().addLast(new HttpObjectAggregator(65536));
		ch.pipeline().addLast(new ChunkedWriteHandler());
		ch.pipeline().addLast(new AcatServerHandler());
	}
}
