package com.mango.acat.core.exception;

public class MethodNotSupportedException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MethodNotSupportedException(String msg){
		super(msg);
	}
}
